import 'package:cloud_firestore/cloud_firestore.dart';

class User{

  String uuid;
  String name;
  String lastName;
  String email;
  String gender;
  String latitud;
  String longitud;
  String path;

  User({this.uuid = "",this.name = "",this.lastName = "",this.gender = "",this.latitud = "",this.longitud = "",this.email = "",this.path = ""});

  User.fromSnapshot(DocumentSnapshot snapshot)
      : uuid = snapshot.documentID,
        name = snapshot['name'],
        lastName = snapshot['lastName'],
        gender = snapshot['gender'],
        email = snapshot['email'],
        path = snapshot['path'],
        longitud = snapshot['longitude'],
        latitud = snapshot['latitude'];

}