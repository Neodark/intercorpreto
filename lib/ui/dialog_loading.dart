import 'package:flutter/material.dart';
import 'package:intercorpreto/ui/overlay_view.dart';

class DialogLoading{

  final BuildContext context;
  bool _dialogOpen = false;
  final bool dismisible;
  String message;


  TextStyle _textStyleLoading = TextStyle(
    color: Colors.white,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
  );

  DialogLoading({this.context,this.dismisible = false,this.message = "Cargando" });

  void show(){
    if(!_dialogOpen) {
      Navigator.push(context, OverlayView(
          dismissible: dismisible,
          content: Container(
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                      height: 60.0,
                      width: 60.0,
                      child: CircularProgressIndicator()),
                  SizedBox(height: 20.0,),
                  Text("Cargando", style: _textStyleLoading,)
                ],
              ),
            ),
          )
      ));
    }
    _dialogOpen = true;
  }

  Future hide({int seconds}){
    if(_dialogOpen) {
      _dialogOpen = false;
      return new Future.delayed(Duration(seconds: seconds == null ? 1 : seconds), () => Navigator.pop(context));
    }
  }

}