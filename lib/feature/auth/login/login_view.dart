import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intercorpreto/feature/auth/code_verification/code_verification_view.dart';
import 'package:intercorpreto/global/custom_color.dart';
import 'package:intercorpreto/global/custom_image.dart';
import 'package:intercorpreto/ui/dialog_loading.dart';
import 'package:intercorpreto/utils/mask_text_input_formatter.dart';
import 'login_bloc.dart';
import 'login_event.dart';
import 'login_state.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  LoginBloc _bloc;
  final _formKey = GlobalKey<FormState>();
  final _controller = new TextEditingController();
  DialogLoading _loading;
  
  var maskFormatter = new MaskTextInputFormatter(
      mask: '+51 ### ### ###', filter: {"#": RegExp(r'[0-9]')});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    if(_loading == null){
      _loading = new DialogLoading(context: context,dismisible: true);
    }
    if(_bloc == null){
      _bloc = new LoginBloc();
      _bloc.controller.stream.listen((String value){
        _loading.hide().whenComplete((){
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => CodeVerificationScreen(verificationId: value,)));
        });
      },onError: (Object error){
        Scaffold.of(context).showSnackBar(new SnackBar(
          content: new Text('Ha ocurrido un error'),
        ));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(CustomImage.BACKGROUND_IMAGE),
            fit: BoxFit.cover
          )
        ),
        child: SafeArea(
          child: BlocListener(
            bloc: _bloc,
            listener: (context,state){
              if(state is LoginLoading){
                _loading.show();
              }
            },
            child: Stack(
              children: <Widget>[
                Center(
                  child: SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 16),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Text(
                            "Bienvenido",
                            style: TextStyle(fontSize: 24,color: Colors.white),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Hero(
                                tag: "splash_logo",
                                child: Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(image: AssetImage(CustomImage.LOGO),fit: BoxFit.cover),
                                    shape: BoxShape.circle,
                                  ),
                                  width: 140,
                                  height: 140,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          TextFormField(
                            controller: _controller,
                            style: TextStyle(
                              color: Colors.white
                            ),
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.number,
                            inputFormatters: [maskFormatter],
                            decoration: InputDecoration(
                              hintText: "Ingresa tu numero de celular",
                              prefixIcon: Icon(Icons.person_outline,color: Colors.white,),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Colors.white.withOpacity(0.5),
                                      style: BorderStyle.solid),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Colors.white.withOpacity(0.5),
                                      style: BorderStyle.solid),
                              ),
                              hintStyle: TextStyle(
                                color: Colors.white
                              ),
                            ),
                            validator: (value){
                              if(value.isEmpty){
                                return "Este campo no puede estar vacío.";
                              }
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 60,
                          ),
                          RaisedButton(onPressed: () => _submit(),
                            child: Text("INICIAR SESION",style: TextStyle(
                              fontSize: 18
                            ),),
                            color: CustomColor.blue,
                            textColor: Colors.white,
                            padding: const EdgeInsets.symmetric(vertical: 18),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _submit(){
    if (_formKey.currentState.validate()) {
      _bloc.dispatch(FetchLogin(phoneNumber: _controller.text));
    }
  }
}
