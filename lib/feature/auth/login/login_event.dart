import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class LoginEvent extends Equatable {
  LoginEvent([List props = const []]) : super(props);
}

class FetchLogin extends LoginEvent {

  final String phoneNumber;

  FetchLogin({@required this.phoneNumber}) : super([phoneNumber]);

  @override
  String toString() => 'FetchLogin';

}