import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LoginState extends Equatable {
  LoginState([List props = const []]) : super(props);
}

class LoginUninitialized extends LoginState {
  @override
  String toString() => 'LoginUninitialized';
}


class LoginLoading extends LoginState {
  @override
  String toString() => 'LoginLoading';
}

class LoginAuthenticated  extends LoginState {

  final String verificationId;

  LoginAuthenticated({this.verificationId = ""});

  @override
  String toString() => 'LoginAuthenticated';
}


class LoginUnauthenticated  extends LoginState {
  @override
  String toString() => 'LoginAuthenticated';
}

class LoginError extends LoginState {

  final String message;

  LoginError({this.message}) : super([message]);

  @override
  String toString() => 'LoginError { message: $message }';

}