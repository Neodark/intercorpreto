import 'dart:async';

import 'package:bloc/bloc.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'login_event.dart';
import 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {

  StreamController<String> controller;

  LoginBloc(){
    controller = new StreamController.broadcast();
  }

  @override
  // TODO: implement initialState
  LoginState get initialState => LoginUninitialized();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    // TODO: implement mapEventToState
    if (event is FetchLogin) {
      yield LoginLoading();
      await FirebaseAuth.instance.verifyPhoneNumber(
          phoneNumber: event.phoneNumber,
          codeAutoRetrievalTimeout: (String verId) {
          },
          codeSent: (String verId, [int forceCodeResend]){
            controller.sink.add(verId);
          },
          timeout: const Duration(seconds: 5),
          verificationCompleted: (user) {
          },
          verificationFailed: (AuthException exception){
            controller.addError(exception.message);
          });

    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.close();
  }

}
