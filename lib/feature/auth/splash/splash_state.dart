import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SplashState extends Equatable {
  SplashState([List props = const []]) : super(props);
}

class SplashUninitialized extends SplashState {
  @override
  String toString() => 'SplashUninitialized';
}


class SplashLoading extends SplashState {
  @override
  String toString() => 'SplashLoading';
}

class SplashSuccess  extends SplashState {

  SplashSuccess();

  @override
  String toString() => 'SplashSuccess';
}

class SplashError extends SplashState {

  final String message;

  SplashError({this.message}) : super([message]);

  @override
  String toString() => 'SplashError { message: $message }';

}