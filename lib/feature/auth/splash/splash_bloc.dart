import 'package:firebase_auth/firebase_auth.dart';
import 'package:intercorpreto/feature/auth/splash/splash_event.dart';
import 'package:intercorpreto/feature/auth/splash/splash_state.dart';
import 'package:bloc/bloc.dart';

class SplashBloc extends Bloc<SplashEvent,SplashState> {

  FirebaseAuth _auth;

  SplashBloc(){
    _auth = FirebaseAuth.instance;
  }

  @override
  // TODO: implement initialState
  SplashState get initialState => SplashUninitialized();

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async*{
    // TODO: implement mapEventToState

  }

  Future<FirebaseUser> getCurrentUser(){
    return _auth.currentUser();
  }

}