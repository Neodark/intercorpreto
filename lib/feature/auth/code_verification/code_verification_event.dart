import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CodeVerificationEvent extends Equatable {
  CodeVerificationEvent([List props = const []]) : super(props);
}

class FetchVerification extends CodeVerificationEvent {

  final String verificationId;
  final String smsCode;

  FetchVerification({@required this.verificationId,@required this.smsCode}) : super([verificationId,smsCode]);

  @override
  String toString() => 'FetchVerification';

}