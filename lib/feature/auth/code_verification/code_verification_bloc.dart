import 'dart:async';

import 'package:bloc/bloc.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'code_verification_event.dart';
import 'code_verification_state.dart';

class CodeVerificationBloc extends Bloc<CodeVerificationEvent, CodeVerificationState> {

  CodeVerificationBloc();

  @override
  // TODO: implement initialState
  CodeVerificationState get initialState => CodeVerificationUninitialized();

  @override
  Stream<CodeVerificationState> mapEventToState(CodeVerificationEvent event) async* {
    // TODO: implement mapEventToState
    if (event is FetchVerification) {
      yield CodeVerificationLoading();
      FirebaseUser user = await FirebaseAuth.instance.currentUser();
      if(user != null){
        yield CodeVerificationSuccess();
      } else {
        final AuthCredential credential = PhoneAuthProvider.getCredential(verificationId: event.verificationId, smsCode: event.smsCode);
        final AuthResult result = await FirebaseAuth.instance.signInWithCredential(credential);
        if(result != null){
          yield CodeVerificationSuccess();
        }else{
          yield CodeVerificationError();
        }
      }
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

}
