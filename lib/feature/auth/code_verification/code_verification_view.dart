import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intercorpreto/feature/splash/splash_view.dart';
import 'package:intercorpreto/feature/user/profile/profile_view.dart';
import 'package:intercorpreto/global/custom_color.dart';
import 'package:intercorpreto/global/custom_image.dart';
import 'package:intercorpreto/ui/dialog_loading.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

import 'code_verification_bloc.dart';
import 'code_verification_event.dart';
import 'code_verification_state.dart';

class CodeVerificationScreen extends StatefulWidget {

  final String verificationId;

  CodeVerificationScreen({Key key, this.verificationId}) : super(key: key);

  @override
  _CodeVerificationScreenState createState() => _CodeVerificationScreenState();
}

class _CodeVerificationScreenState extends State<CodeVerificationScreen> {

  bool hasError = false;
  TextEditingController _controller;
  CodeVerificationBloc _bloc;
  DialogLoading _loading;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = new TextEditingController();
    _bloc = CodeVerificationBloc();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    if(_loading == null){
      _loading = new DialogLoading(context: context,dismisible: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(CustomImage.BACKGROUND_IMAGE),
              fit: BoxFit.cover
          )
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: BlocListener(
          bloc: _bloc,
          listener: (context,state){
            if(state is CodeVerificationLoading){
              _loading.show();
            }
            if(state is CodeVerificationSuccess){
              _loading.hide().whenComplete((){
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => SplashScreen()));
              });
            }
          },
          child: SafeArea(
            child: Stack(
              children: <Widget>[
                Center(
                  child: SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 16),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Ingrese el código que fue enviado",textAlign: TextAlign.center,style: TextStyle(
                            fontSize: 16,
                            color: Colors.white
                        ),),
                        SizedBox(height: 40,),
                        PinCodeTextField(
                          autofocus: false,
                          controller: _controller,
                          hideCharacter: false,
                          highlight: true,
                          highlightColor: CustomColor.blue,
                          defaultBorderColor: Colors.white,
                          hasTextBorderColor: Colors.white,
                          maxLength: 6,
                          hasError: hasError,
                          onTextChanged: (text) {
                            setState(() {
                              hasError = false;
                            });
                          },
                          onDone: (text){
                            print("DONE $text");
                          },
                          pinBoxWidth: 50,
                          pinCodeTextFieldLayoutType: PinCodeTextFieldLayoutType.AUTO_ADJUST_WIDTH,
                          wrapAlignment: WrapAlignment.start,
                          pinBoxDecoration: ProvidedPinBoxDecoration.underlinedPinBoxDecoration,
                          pinTextStyle: TextStyle(fontSize: 30.0,color: Colors.white),
                          pinTextAnimatedSwitcherTransition: ProvidedPinBoxTextAnimation.scalingTransition,
                          pinTextAnimatedSwitcherDuration: Duration(milliseconds: 300),
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        FlatButton(onPressed: () => _submit(),
                          child: Text("SIGUIENTE"),
                          color: Colors.blue,
                          textColor: Colors.white,
                          padding: const EdgeInsets.symmetric(vertical: 18),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _submit(){
    _bloc.dispatch(FetchVerification(smsCode: _controller.text,verificationId: widget.verificationId));
  }
}
