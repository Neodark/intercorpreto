import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CodeVerificationState extends Equatable {
  CodeVerificationState([List props = const []]) : super(props);
}

class CodeVerificationUninitialized extends CodeVerificationState {
  @override
  String toString() => 'CodeVerificationUninitialized';
}


class CodeVerificationLoading extends CodeVerificationState {
  @override
  String toString() => 'CodeVerificationLoading';
}

class CodeVerificationSuccess extends CodeVerificationState {


  CodeVerificationSuccess();

  @override
  String toString() => 'CodeVerificationSuccess';
}

class CodeVerificationError extends CodeVerificationState {

  final String message;

  CodeVerificationError({this.message}) : super([message]);

  @override
  String toString() => 'CodeVerificationError { message: $message }';

}