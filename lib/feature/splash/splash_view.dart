import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intercorpreto/feature/auth/login/login_view.dart';
import 'package:intercorpreto/feature/splash/splash_bloc.dart';
import 'package:intercorpreto/feature/splash/splash_event.dart';
import 'package:intercorpreto/feature/splash/splash_state.dart';
import 'package:intercorpreto/feature/user/profile/profile_view.dart';
import 'package:intercorpreto/feature/user/user_create/user_create_view.dart';
import 'package:intercorpreto/global/custom_image.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  SplashBloc _bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    if(_bloc == null){
      _bloc = SplashBloc();
      _bloc.dispatch(FetchVerifyUser());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(CustomImage.BACKGROUND_IMAGE),
                fit: BoxFit.cover
            )
        ),
        child: SafeArea(
          child: BlocListener(
            bloc: _bloc,
            listener: (context,state){
              if(state is SplashLogin){
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => LoginScreen()));
              }
              if(state is SplashMain){
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => ProfileScreen()));
              }
              if(state is SplashCreateUser){
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => UserCreateScreen()));
              }
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 4,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Hero(
                        tag: "splash_logo",
                        child: Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(image: AssetImage(CustomImage.LOGO),fit: BoxFit.cover),
                            shape: BoxShape.circle,
                          ),
                          width: 180,
                          height: 180,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Cargando",style: TextStyle(
                          color: Colors.white,
                          fontSize: 20
                      ),)
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
