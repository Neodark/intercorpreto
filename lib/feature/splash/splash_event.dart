import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class SplashEvent extends Equatable {
  SplashEvent([List props = const []]) : super(props);
}

class FetchVerifyUser extends SplashEvent {

  FetchVerifyUser() : super([]);

  @override
  String toString() => 'FetchVerifyUser';

}