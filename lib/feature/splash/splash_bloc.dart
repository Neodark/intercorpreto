import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:intercorpreto/feature/splash/splash_event.dart';
import 'package:intercorpreto/feature/splash/splash_state.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  SplashBloc();

  @override
  // TODO: implement initialState
  SplashState get initialState => SplashUninitialized();

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    // TODO: implement mapEventToState
    if (event is FetchVerifyUser) {
      var currentUser = await FirebaseAuth.instance.currentUser();
      if (currentUser == null) {
        yield SplashLogin();
      } else {
        DocumentSnapshot snapshot = await Firestore.instance
            .collection("users")
            .document(currentUser.uid)
            .get();
        if (snapshot.exists) {
          yield SplashMain();
        } else {
          yield SplashCreateUser();
        }
      }
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
