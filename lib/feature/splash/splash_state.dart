import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SplashState extends Equatable {
  SplashState([List props = const []]) : super(props);
}

class SplashUninitialized extends SplashState {
  @override
  String toString() => 'SplashUninitialized';
}


class SplashLoading extends SplashState {
  @override
  String toString() => 'SplashLoading';
}

class SplashLogin  extends SplashState {

  SplashLogin();

  @override
  String toString() => 'SplashLogin';
}

class SplashMain  extends SplashState {

  SplashMain();

  @override
  String toString() => 'SplashMain';
}

class SplashCreateUser  extends SplashState {

  SplashCreateUser();

  @override
  String toString() => 'SplashCreateUser';
}

class SplashError extends SplashState {

  final String message;

  SplashError({this.message}) : super([message]);

  @override
  String toString() => 'SplashError { message: $message }';

}