import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class ProfileEvent extends Equatable {
  ProfileEvent([List props = const []]) : super(props);
}

class FetchUser extends ProfileEvent {

  FetchUser() : super([]);

  @override
  String toString() => 'FetchUser';

}