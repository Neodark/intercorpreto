import 'package:equatable/equatable.dart';
import 'package:intercorpreto/model/user.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProfileState extends Equatable {
  ProfileState([List props = const []]) : super(props);
}

class ProfileUninitialized extends ProfileState {
  @override
  String toString() => 'ProfileUninitialized';
}


class ProfileLoading extends ProfileState {
  @override
  String toString() => 'ProfileLoading';
}

class ProfileSuccess  extends ProfileState {

  final User user;

  ProfileSuccess({this.user});

  @override
  String toString() => 'ProfileSuccess';
}

class ProfileError extends ProfileState {

  final String message;

  ProfileError({this.message}) : super([message]);

  @override
  String toString() => 'ProfileError { message: $message }';

}