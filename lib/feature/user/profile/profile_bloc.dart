import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:intercorpreto/feature/splash/splash_state.dart';
import 'package:intercorpreto/feature/user/profile/profile_event.dart';
import 'package:intercorpreto/feature/user/profile/profile_state.dart';
import 'package:intercorpreto/model/user.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {

  ProfileBloc();

  @override
  // TODO: implement initialState
  ProfileState get initialState => ProfileUninitialized();

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    // TODO: implement mapEventToState
    if (event is FetchUser) {
      var currentUser = await FirebaseAuth.instance.currentUser();
      if (currentUser == null) {
        yield ProfileError();
      } else {
        DocumentSnapshot snapshot = await Firestore.instance
            .collection("users")
            .document(currentUser.uid)
            .get();
        if (snapshot.exists) {
          yield ProfileSuccess(user: User.fromSnapshot(snapshot));
        } else {
          yield ProfileError();
        }
      }
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
