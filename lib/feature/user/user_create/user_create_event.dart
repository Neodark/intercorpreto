import 'dart:io';

import 'package:intercorpreto/model/user.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class UserCreateEvent extends Equatable {
  UserCreateEvent([List props = const []]) : super(props);
}

class FetchCreateUser extends UserCreateEvent {

  final User user;
  final File file;

  FetchCreateUser({@required this.user,@required this.file}) : super([user,file]);

  @override
  String toString() => 'FetchCreateUser';

}