import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart' as Path;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intercorpreto/feature/user/profile/profile_view.dart';
import 'package:intercorpreto/feature/user/user_create/user_create_state.dart';
import 'package:intercorpreto/ui/dialog_loading.dart';
import 'package:location/location.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intercorpreto/feature/user/user_create/user_create_bloc.dart';
import 'package:intercorpreto/feature/user/user_create/user_create_event.dart';
import 'package:intercorpreto/global/custom_color.dart';
import 'package:intercorpreto/global/custom_image.dart';
import 'package:intercorpreto/model/user.dart';

class UserCreateScreen extends StatefulWidget {
  @override
  _UserCreateScreenState createState() => _UserCreateScreenState();
}

class _UserCreateScreenState extends State<UserCreateScreen>{

  String _genero;
  List<String> generoList;
  User _user;
  File _file;
  DialogLoading _loading;

  final _formKey = GlobalKey<FormState>();
  FocusNode emailFocusNode = FocusNode();
  FocusNode nameFocusNode = FocusNode();
  FocusNode lastNameFocusNode = FocusNode();
  TextEditingController _controller = new TextEditingController();
  TextEditingController _controllerImage = new TextEditingController();
  UserCreateBloc _bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bloc = new UserCreateBloc();
    generoList = new List<String>();
    _user = new User();
    initData();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    if(_loading == null){
      _loading = new DialogLoading(context: context,dismisible: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(CustomImage.BACKGROUND_IMAGE),
                fit: BoxFit.cover
            )
        ),
        child: Stack(
          children: <Widget>[
            Center(
              child: SingleChildScrollView(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: BlocListener(
                  bloc: _bloc,
                  listener: (context,state){
                    if(state is UserCreateLoading){
                      _loading.show();
                    }
                    if(state is UserCreateSuccess){
                      _loading.hide().whenComplete((){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => ProfileScreen()));
                      });
                    }
                  },
                  child: SafeArea(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          SizedBox(height: 50,),
                          Text(
                            "Crear Usuario",
                            style: TextStyle(fontSize: 24,color: Colors.white),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: 40,),
                          TextFormField(
                            style: TextStyle(
                                color: Colors.white
                            ),
                            focusNode: emailFocusNode,
                            textInputAction: TextInputAction.next,
                            keyboardType: TextInputType.emailAddress,
                            onFieldSubmitted: (value){
                              FocusScope.of(context).requestFocus(nameFocusNode);
                            },
                            decoration: InputDecoration(
                              hintText: "Correo electrónico",
                              prefixIcon: Icon(Icons.email,color: Colors.white,),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Colors.white.withOpacity(0.5),
                                    style: BorderStyle.solid),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Colors.white.withOpacity(0.5),
                                    style: BorderStyle.solid),
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.white
                              ),
                            ),
                            onSaved: (value){
                              _user.email = value;
                            },
                            validator: (value){
                              String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                              RegExp regExp = new RegExp(p);
                              if(value.isEmpty){
                                return "Este campo no puede estar vacío.";
                              }
                              if(!regExp.hasMatch(value)){
                                return "Debe ingresar un correo válido";
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 20,),
                          TextFormField(
                            style: TextStyle(
                                color: Colors.white
                            ),
                            textInputAction: TextInputAction.next,
                            keyboardType: TextInputType.text,
                            focusNode: nameFocusNode,
                            onFieldSubmitted: (value){
                              FocusScope.of(context).requestFocus(lastNameFocusNode);
                            },
                            onSaved: (value){
                              _user.name = value;
                            },
                            decoration: InputDecoration(
                              hintText: "Nombres",
                              prefixIcon: Icon(Icons.person_outline,color: Colors.white,),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Colors.white.withOpacity(0.5),
                                    style: BorderStyle.solid),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Colors.white.withOpacity(0.5),
                                    style: BorderStyle.solid),
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.white
                              ),
                            ),
                            validator: (value){
                              if(value.isEmpty){
                                return "Este campo no puede estar vacío.";
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 20,),
                          TextFormField(
                            style: TextStyle(
                                color: Colors.white
                            ),
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.text,
                            focusNode: lastNameFocusNode,
                            onSaved: (value){
                              _user.lastName = value;
                            },
                            decoration: InputDecoration(
                              hintText: "Apellidos",
                              prefixIcon: Icon(Icons.person_outline,color: Colors.white,),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Colors.white.withOpacity(0.5),
                                    style: BorderStyle.solid),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Colors.white.withOpacity(0.5),
                                    style: BorderStyle.solid),
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.white
                              ),
                            ),
                            validator: (value){
                              if(value.isEmpty){
                                return "Este campo no puede estar vacío.";
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 20,),
                          new DropdownButtonFormField<String>(
                            decoration: InputDecoration(
                              prefixIcon: Icon(Icons.person,color: Colors.white,),
                              suffixIcon: Icon(Icons.arrow_drop_down,color: Colors.white,),
                              border: UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Colors.white.withOpacity(0.5),
                                    style: BorderStyle.solid),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Colors.white.withOpacity(0.5),
                                    style: BorderStyle.solid),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Colors.white.withOpacity(0.5),
                                    style: BorderStyle.solid),
                              ),
                            ),
                            onSaved: (value){
                              _user.gender = value;
                            },
                            validator: (value){
                              if(value == null){
                                return "Este campo no puede estar vacío.";
                              }
                              return null;
                            },
                            hint: new Text("Género",style: TextStyle(color: Colors.white,fontSize: 16),),
                            value: _genero,
                            onChanged: (String newValue) {
                              setState(() {
                                _genero = newValue;
                              });
                            },
                            items: generoList.map((String item) {
                              return new DropdownMenuItem<String>(
                                value: item,
                                child: new Text(
                                  item,
                                ),
                              );
                            }).toList(),
                          ),
                          SizedBox(height: 20,),
                          new Row(
                            children: <Widget>[
                              Expanded(
                                child:
                                TextFormField(
                                  style: TextStyle(
                                      color: Colors.white
                                  ),
                                  readOnly: true,
                                  controller: _controller,
                                  textInputAction: TextInputAction.next,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    hintText: "Geolocalización",
                                    prefixIcon: Icon(Icons.add_location,color: Colors.white,),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: Colors.white.withOpacity(0.5),
                                          style: BorderStyle.solid),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: Colors.white.withOpacity(0.5),
                                          style: BorderStyle.solid),
                                    ),
                                    hintStyle: TextStyle(
                                        color: Colors.white
                                    ),
                                  ),
                                  validator: (value){
                                    if(value.isEmpty){
                                      return "Este campo no puede estar vacío.";
                                    }
                                    return null;
                                  },
                                ),
                              ),
                              SizedBox(width: 5,),
                              FlatButton(
                                padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                                color: CustomColor.sky,
                                child: Icon(Icons.location_searching,color: Colors.white,),
                                onPressed: () => _getCurrentLocation(),
                              )
                            ],
                          ),
                          SizedBox(height: 20,),
                          new Row(
                            children: <Widget>[
                              Expanded(
                                child:
                                TextFormField(
                                  style: TextStyle(
                                      color: Colors.white
                                  ),
                                  readOnly: true,
                                  controller: _controllerImage,
                                  textInputAction: TextInputAction.next,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    hintText: "Imagen Perfil",
                                    prefixIcon: Icon(Icons.image,color: Colors.white,),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: Colors.white.withOpacity(0.5),
                                          style: BorderStyle.solid),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: Colors.white.withOpacity(0.5),
                                          style: BorderStyle.solid),
                                    ),
                                    hintStyle: TextStyle(
                                        color: Colors.white
                                    ),
                                  ),
                                  validator: (value){
                                    if(value == null){
                                      return "Este campo no puede estar vacío.";
                                    }
                                    return null;
                                  },
                                ),
                              ),
                              SizedBox(width: 5,),
                              FlatButton(
                                padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                                color: CustomColor.sky,
                                child: Icon(Icons.attach_file,color: Colors.white,),
                                onPressed: () => _getImage(),
                              )
                            ],
                          ),
                          SizedBox(height: 40,),
                          FlatButton(
                            child: Text("CREAR USUARIO",style: TextStyle(color: Colors.white),),
                            color: CustomColor.blue,
                            padding: const EdgeInsets.symmetric(vertical: 20),
                            onPressed: () => _summit(),
                          ),
                          SizedBox(height: 50,),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _getCurrentLocation() async{
    var location = new Location();
    try {
      await location.getLocation().then((currentLocation){
        _user.latitud = currentLocation.latitude.toString();
        _user.longitud = currentLocation.longitude.toString();
        setState(() {
          _controller.text = _user.latitud+","+_user.longitud;
        });
      });
    } on PlatformException catch (e) {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text('Ha ocurrido un error'),
      ));
    }
  }

  _getImage() async{
    await ImagePicker.pickImage(source: ImageSource.gallery).then((image) {
      setState(() {
        _file = image;
        _controllerImage.text = Path.basename(image.path);
      });
    });
  }


  _summit(){
    if (_formKey.currentState.validate() && _file != null) {
      _formKey.currentState.save();
      _bloc.dispatch(FetchCreateUser(user: _user,file: _file));
    }else{
      if(_file == null){
        Scaffold.of(context).showSnackBar(new SnackBar(
          content: new Text('Por favor elija una imagen para poder continuar.'),
        ));
      }
    }
  }

  void initData() {
    generoList.add("Femenino");
    generoList.add("Masculino");
  }

}
