import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:path/path.dart' as Path;
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:intercorpreto/feature/user/user_create/user_create_event.dart';
import 'package:intercorpreto/feature/user/user_create/user_create_state.dart';

class UserCreateBloc extends Bloc<UserCreateEvent, UserCreateState> {
  UserCreateBloc();

  @override
  // TODO: implement initialState
  UserCreateState get initialState =>  UserCreateUninitialized();

  @override
  Stream<UserCreateState> mapEventToState(UserCreateEvent event) async* {
    // TODO: implement mapEventToState
    if (event is FetchCreateUser) {
      yield UserCreateLoading();
      FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();
      StorageReference storageReference = FirebaseStorage.instance
          .ref()
          .child('users/${DateTime.now().millisecondsSinceEpoch.toString()+Path.basename(event.file.path)}');
      StorageUploadTask uploadTask = storageReference.putFile(event.file);
      await uploadTask.onComplete;
      String url = await storageReference.getDownloadURL();
      Firestore.instance
          .collection("users")
          .document(currentUser.uid)
          .setData({
        "name" : event.user.name,
        "lastName" :  event.user.lastName,
        "gender" :  event.user.gender,
        "email" :  event.user.email,
        "latitude" :  event.user.latitud,
        "longitude" :  event.user.longitud,
        "path": url
      });
      yield UserCreateSuccess();
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
