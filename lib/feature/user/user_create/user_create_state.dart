import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UserCreateState extends Equatable {
  UserCreateState([List props = const []]) : super(props);
}

class UserCreateUninitialized extends UserCreateState {
  @override
  String toString() => 'UserCreateUninitialized';
}


class UserCreateLoading extends UserCreateState {
  @override
  String toString() => 'UserCreateLoading';
}

class UserCreateSuccess  extends UserCreateState {


  UserCreateSuccess();

  @override
  String toString() => 'UserCreateSuccess';
}

class UserCreateError extends UserCreateState {

  final String message;

  UserCreateError({this.message}) : super([message]);

  @override
  String toString() => 'UserCreateError { message: $message }';

}