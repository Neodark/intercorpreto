import 'dart:ui';

class CustomColor{

  static const Color blue = const Color.fromRGBO(74, 193, 254,1.0);
  static const Color sky = const Color.fromRGBO(75, 117, 255,1.0);
  static const Color bluem = const Color.fromRGBO(63, 73, 150,1.0);
  static const Color skym = const Color.fromRGBO(103, 210, 254,1.0);
}