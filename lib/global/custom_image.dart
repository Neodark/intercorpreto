class CustomImage {
  static final String PATH = "assets/img/";

  static final String BACKGROUND_IMAGE = PATH+"background.jpg";
  static final String LOGO = PATH+"logo.png";
}